﻿using System;
using System.Data;
using System.Windows.Forms;

using GoldingsKeysDAL;

namespace GoldingsKeysWindowsForms
{
    public partial class Search : Form
    {
        static string parsedKeyCode;
        static readonly string connStr = UtilityDAL.GetConnectionString();

        static readonly string databaseEnvironment = UtilityDAL.ExtractFromConnString();

        //static string useXMLFlag = System.Configuration.ConfigurationManager.AppSettings["UseXMLDatabase"];
        //static bool useEmbeddedXMLAsDatabase = AppSettings.Get<bool>("useEmbeddedXMLAsDatabase");
        static readonly bool useEmbeddedXMLAsDatabase = UtilityDAL.AppSettings.Get<bool>("useEmbeddedXMLAsDatabase");

        // Data set is small enough to get all data when the form initially loads
        // Filtering is done client side. This means the data source is only hit once.
        private static DataView dv;

        public Search()
        {
            InitializeComponent();
            LoadData();
            DisplayDataSource();
        }

        private void LoadData()
        {
            KeysDAL keysDAL = new KeysDAL();

            if (useEmbeddedXMLAsDatabase)
            {
                dv = new DataView(keysDAL.GetAllKeysXML());
            }
            else
            {
                keysDAL.OpenConnection(connStr);
                // Pre-filtered data not necessary
                // DataTable is small enough that we can load the whole thing and filter client side
                //dt = keysDAL.GetFilteredKeysDb(parsedKeyCode);
                dv = new DataView(keysDAL.GetAllKeysDb());
                keysDAL.CloseConnection();
            }
        }

        private void DisplayDataSource()
        {
            if (useEmbeddedXMLAsDatabase)
                lblDataSource.Text = "Data Source = Embedded XML";
            else
                lblDataSource.Text = databaseEnvironment;
        }
      
        private void btnSearch_Click(object sender, EventArgs e)
        {
            // Clear out the contents of any previous search
            dgvKeys.ClearSelection();

            // Change mouse pointer
            Cursor.Current = Cursors.WaitCursor;

            if (isValidKeyCode())
            {                
                if (chkWildcard.Checked)
                    parsedKeyCode = $"%{txtKeyCode.Text}%";
                else
                    parsedKeyCode = txtKeyCode.Text;

                // Filter data
                string filterStr = $"KeyCode like '{parsedKeyCode}'";
                dv.RowFilter = filterStr;

                // Need to check whether Data View contains records after filtering
                if (dv.Count == 0)
                {
                    lblNumRecords.Text = "Key Code not found";
                    this.dgvKeys.DataSource = null;
                }
                else
                {
                    lblNumRecords.Text = $"{dv.Count} Matches for {parsedKeyCode}";
                    // Dump the view into the grid
                    this.dgvKeys.DataSource = dv;
                }
                // Return mouse pointer to normal
                Cursor.Current = Cursors.Default;
            }
            else
            {
                // An invalid KeyCode has been entered so clear the text box
                txtKeyCode.Clear();
            }
        }

        private bool isValidKeyCode()
        {
            if (txtKeyCode.Text == "")
            {
                MessageBox.Show("Please specify a Key Code.");
                return false;
            }
            else if (!char.IsLetter(txtKeyCode.Text[0]))
            {
                MessageBox.Show("First character of the Key Code must be a letter.");
                return false;
            }
            // Check there is a 2nd character
            else if ((txtKeyCode.TextLength > 1))
            {
                // Check 2nd character is not numeric
                if (char.IsLetter(txtKeyCode.Text[1]))
                {
                    // 2nd character is text
                    return true;
                }
                else
                {
                    MessageBox.Show("The first two characters of the Key Code must be letters.");
                    return false;
                }
            }
            else
                return true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
