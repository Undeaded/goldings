﻿namespace GoldingsKeysWindowsForms
{
    partial class Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtKeyCode = new System.Windows.Forms.TextBox();
            this.grpSearch = new System.Windows.Forms.GroupBox();
            this.lblKeyCode = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.grpResults = new System.Windows.Forms.GroupBox();
            this.lblNumRecords = new System.Windows.Forms.Label();
            this.dgvKeys = new System.Windows.Forms.DataGridView();
            this.chkWildcard = new System.Windows.Forms.CheckBox();
            this.lblDataSource = new System.Windows.Forms.Label();
            this.grpSearch.SuspendLayout();
            this.grpResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKeys)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(323, 25);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(94, 63);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtKeyCode
            // 
            this.txtKeyCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtKeyCode.Location = new System.Drawing.Point(123, 31);
            this.txtKeyCode.MaxLength = 30;
            this.txtKeyCode.Name = "txtKeyCode";
            this.txtKeyCode.Size = new System.Drawing.Size(168, 26);
            this.txtKeyCode.TabIndex = 1;
            // 
            // grpSearch
            // 
            this.grpSearch.Controls.Add(this.lblDataSource);
            this.grpSearch.Controls.Add(this.lblKeyCode);
            this.grpSearch.Controls.Add(this.btnExit);
            this.grpSearch.Controls.Add(this.grpResults);
            this.grpSearch.Controls.Add(this.chkWildcard);
            this.grpSearch.Controls.Add(this.txtKeyCode);
            this.grpSearch.Controls.Add(this.btnSearch);
            this.grpSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSearch.Location = new System.Drawing.Point(22, 22);
            this.grpSearch.Name = "grpSearch";
            this.grpSearch.Size = new System.Drawing.Size(798, 528);
            this.grpSearch.TabIndex = 0;
            this.grpSearch.TabStop = false;
            this.grpSearch.Text = "Key Search";
            // 
            // lblKeyCode
            // 
            this.lblKeyCode.AutoSize = true;
            this.lblKeyCode.Location = new System.Drawing.Point(17, 34);
            this.lblKeyCode.Name = "lblKeyCode";
            this.lblKeyCode.Size = new System.Drawing.Size(100, 20);
            this.lblKeyCode.TabIndex = 6;
            this.lblKeyCode.Text = "Key Code : ";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(701, 464);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 39);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // grpResults
            // 
            this.grpResults.Controls.Add(this.lblNumRecords);
            this.grpResults.Controls.Add(this.dgvKeys);
            this.grpResults.Location = new System.Drawing.Point(20, 94);
            this.grpResults.Name = "grpResults";
            this.grpResults.Size = new System.Drawing.Size(756, 354);
            this.grpResults.TabIndex = 4;
            this.grpResults.TabStop = false;
            this.grpResults.Text = "Results";
            // 
            // lblNumRecords
            // 
            this.lblNumRecords.AutoSize = true;
            this.lblNumRecords.Location = new System.Drawing.Point(19, 25);
            this.lblNumRecords.Name = "lblNumRecords";
            this.lblNumRecords.Size = new System.Drawing.Size(97, 20);
            this.lblNumRecords.TabIndex = 4;
            this.lblNumRecords.Text = "< Status > ";
            // 
            // dgvKeys
            // 
            this.dgvKeys.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvKeys.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKeys.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvKeys.Location = new System.Drawing.Point(22, 57);
            this.dgvKeys.Name = "dgvKeys";
            this.dgvKeys.Size = new System.Drawing.Size(713, 281);
            this.dgvKeys.TabIndex = 3;
            // 
            // chkWildcard
            // 
            this.chkWildcard.AutoSize = true;
            this.chkWildcard.Location = new System.Drawing.Point(123, 64);
            this.chkWildcard.Name = "chkWildcard";
            this.chkWildcard.Size = new System.Drawing.Size(97, 24);
            this.chkWildcard.TabIndex = 1;
            this.chkWildcard.Text = "Wildcard";
            this.chkWildcard.UseVisualStyleBackColor = true;
            // 
            // lblDataSource
            // 
            this.lblDataSource.AutoSize = true;
            this.lblDataSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataSource.Location = new System.Drawing.Point(41, 478);
            this.lblDataSource.Name = "lblDataSource";
            this.lblDataSource.Size = new System.Drawing.Size(76, 13);
            this.lblDataSource.TabIndex = 8;
            this.lblDataSource.Text = "Data Source =";
            // 
            // Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(837, 564);
            this.Controls.Add(this.grpSearch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Search";
            this.Text = "Search";
            this.grpSearch.ResumeLayout(false);
            this.grpSearch.PerformLayout();
            this.grpResults.ResumeLayout(false);
            this.grpResults.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKeys)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtKeyCode;
        private System.Windows.Forms.GroupBox grpSearch;
        private System.Windows.Forms.CheckBox chkWildcard;
        private System.Windows.Forms.GroupBox grpResults;
        private System.Windows.Forms.Label lblNumRecords;
        private System.Windows.Forms.DataGridView dgvKeys;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lblKeyCode;
        private System.Windows.Forms.Label lblDataSource;
    }
}