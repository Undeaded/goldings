﻿using System;
using System.Windows.Forms;

namespace GoldingsKeysWindowsForms
{
    public partial class Navigation : Form
    {
        public Navigation()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form frmSearch = new Search();
            frmSearch.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
