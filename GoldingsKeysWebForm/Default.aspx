﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GoldingsKeysWebForm.Default" %>

<!DOCTYPE html>
<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link id="cssStyle" runat="server" rel="stylesheet" type="text/css" />
    <style>

        /**************************************************************/
        /* NOTE - Shared Css is defined here                          */
        /* See Desktop.css and Mobile.css for device specific classes */
        /**************************************************************/

        .datasource-label {
            font-style: italic;
            margin:1%;
        }
        .datasource-label--size {
            font-size: xx-small;
        }
        .edward {
            /*Font will be inherited from body*/
            /*font-family: "Edwardian Script ITC", Times, serif;*/
            font-weight: bold;
            text-align: center;
            text-decoration: underline;
        }
        .flex {
            align-items: stretch;
            display: flex;
            flex-direction: row;
            flex-wrap: nowrap;
            justify-content: space-around;
        }
        .gridview-header {
            text-decoration: underline;
        }
        .gridview {
            background-color: #FCFDD7;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
        .gridview tr td {
            padding:10px;
        }
        .gridview th {
            padding:10px;
        }
        .header {
            background-color:#FCFDD7;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            width:55%;
        }
        .keycode-panel {
            background-color: #FCFDD7;
            border-style: groove;
        }
        .keycode-table {
            margin: auto;
            text-align: center;
        }
        .label {
            background-color: #FCFDD7;
            display: inline-block;
            font-weight: bold;
            margin-bottom: 1%;
            margin-left: 3%;
            margin-right: 3%;
            padding: 1.5%;
        }
        .label:empty {
            padding: 0%;
        }
        .label-table {
            margin: auto;
            text-align: center;
            width: 70%;
        }
        .keysimage {
            background-image: url("keys4.png");
        }
        .margin-small {
            margin:1%;
        }
        .output-panel {
            margin:1%;
            text-align: center;
        }
        .reports-control {
            margin-bottom: 3%;
            margin-left: 3%;
            margin-right: 3%;
        }
        .reports-panel {
            background-color: #FCFDD7;
            border-style: groove;
        }
        .reports-table {
            margin: auto;
            text-align: center;
        }
        
        .selectedfont {
            font-family: "Francois One";
        }
        input, textarea, select {font-family: inherit}

        h1 {
            font-size: 2.5em; /* 40px/16=2.5em */
        }
        h2 {
            font-size: 1.875em; /* 30px/16=1.875em */
        }
        p {
            font-size: 0.875em; /* 14px/16=0.875em */
        }
    </style>
    <title>Golding's KeyFinder General</title>
</head>
<body class="keysimage selectedfont">
    <form id="form1" runat="server">
    <div id="header" class="header">
        <hr/>
        <h1 class="edward">Golding's KeyFinder General</h1>
        <table class="label-table">
            <tr>
                <td><asp:Label ID="lblDataSource" runat="server" CssClass="datasource-label datasource-label--size"> </asp:Label></td>
            </tr>
        </table>
        <hr/>
    </div>
    <div class="flex">
    <asp:Panel ID="pnlKeyCode" runat="server" CssClass="keycode-panel keycode-panel--size">
        <table id="tblKeyCode" class="keycode-table keycode-table--size">
            <tr>
                <td>
                    <asp:Label ID="lblKeyCode" runat="server" Text="Key Code:" CssClass="keycode-control"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtKeyCode" runat="server" onkeyup="this.value=this.value.toUpperCase()" CssClass="keycode-control keycode-control--textbox"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSearchType" runat="server" CssClass="keycode-control" Text="Search Type:"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList ID="lstSearchType" runat="server" CssClass="keycode-control" OnSelectedIndexChanged="lstSearchType_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" CssClass="button" />
                </td>
            </tr>
        </table>       
    </asp:Panel>
        <asp:Panel ID="pnlReports" runat="server" CssClass="reports-panel reports-panel--size">
        <table id="tblReports" class="reports-table reports-table--size">
            <tr>
                <td>
                    <asp:Label ID="lblReports" runat="server" CssClass="reports-control reports-control--size reports-control--margintop" Text="Reports:"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList ID="lstReports" runat="server" CssClass="reports-control reports-control--size reports-control--margintop">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <!-- Filler is here just to preserve size of panel ie keep same size as other panel -->
                <td>
                    <asp:Label ID="lblFiller" runat="server" CssClass="reports-control reports-control--size reports-control--margintop" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnReport" runat="server" Text="Run Report" OnClick="btnReport_click" CssClass="button" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    </div>
    <hr/>
    <br/>
        <table id="tblMessage" class="label-table">
        <tr>
            <td><asp:Label ID="lblMessage" runat="server" CssClass="label label--margintop label--size"></asp:Label></td>
        </tr>
    </table>
    <asp:Panel ID="pnlOutput" runat="server" CssClass="output-panel">
        <br />
        <asp:Label ID="lblResults" runat="server" CssClass="label label--margintop label--size"></asp:Label>
        <p></p>
        <p></p>
        <asp:GridView ID="grdKeys" runat="server" CssClass="gridview gridview--size">
            <HeaderStyle CssClass="gridview-header" />
        </asp:GridView>
        <p></p>
    </asp:Panel>    
</form>
</body>
</html>