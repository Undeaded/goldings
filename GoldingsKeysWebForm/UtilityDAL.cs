﻿using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;

namespace GoldingsKeysWebForm
{
    internal static class UtilityDAL
    {
        // Get the connection string from config file
        internal static string GetConnectionString()
        {
            // Assume failure.
            string returnValue = null;
            
            // Generic Db name that will be swapped out by Web.config transformation
            ConnectionStringSettings Settings = ConfigurationManager.ConnectionStrings["Goldings"];

            if (Settings != null)
                returnValue = Settings.ConnectionString;          
            return returnValue;
        }

        public static class AppSettings
        {
            public static T Get<T>(string key)
            {
                // Handy AppSettings Helper...
                // Implemented ConfigurationErrorsException rather than set up user defined exception (AppSettingNotFoundException)
                // https://stacktoheap.com/blog/2013/01/20/using-typeconverters-to-get-appsettings-in-net/
                var appSetting = ConfigurationManager.AppSettings[key];
                //if (string.IsNullOrWhiteSpace(appSetting)) throw new AppSettingNotFoundException(key);
                if (string.IsNullOrWhiteSpace(appSetting)) throw new ConfigurationErrorsException(key);
                var converter = TypeDescriptor.GetConverter(typeof(T));
                return (T)(converter.ConvertFromInvariantString(appSetting));
            }
        }

        internal static string ExtractFromConnString()
        {
            // Assume failure.
            string strExtract = "N/A";
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["Goldings"];
            if (settings != null)
            {
                // Easier to manipulate connection string with dedicated class ie SqlConnectionStringBuilder
                // https://stackoverflow.com/questions/11803518/extract-properties-of-sql-connection-string
                SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder(settings.ConnectionString);                
                strExtract = $"[ SERVER: '{csb.DataSource}', DATABASE: '{csb.InitialCatalog}' ]" ;
            }
            return strExtract;
        }

        // 20181024 Removed Embedded LocalDb Database (embedding xml is a better solution)
        // Left the code here for reference...
        //
        //internal static string GetConnectionStringEmbedded()
        //{
        //    // Assume failure.
        //    string strConnRelative = null;

        //    // Look for the name in config file
        //    ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["GoldingsLocalNoPath"];

        //    if (settings != null)
        //    {
        //        // Easier to manipulate connection string with dedicated class ie SqlConnectionStringBuilder
        //        // https://stackoverflow.com/questions/11803518/extract-properties-of-sql-connection-string
        //        SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder(settings.ConnectionString);

        //        // Add in the relative path...
        //        // https://stackoverflow.com/questions/6041332/best-way-to-get-application-folder-path
        //        csb.AttachDBFilename = AppDomain.CurrentDomain.BaseDirectory + csb.AttachDBFilename;

        //        strConnRelative = csb.ConnectionString;
        //    }

        //    return strConnRelative;
        //}

    }
}
