﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using GoldingsKeysDAL;

namespace GoldingsKeysWebForm
{
    public partial class Default : System.Web.UI.Page
    {
        static string parsedKeyCode;
        static bool blnReportUsesOwnDataSet;
        static readonly bool blnUseEmbeddedXMLAsDatabase = UtilityDAL.AppSettings.Get<bool>("UseEmbeddedXMLAsDatabase");
        static readonly bool blnForceMobile = UtilityDAL.AppSettings.Get<bool>("ForceMobile");
        static readonly string databaseEnvironment = UtilityDAL.ExtractFromConnString();
        static readonly string connStr = UtilityDAL.GetConnectionString();

        // DataSet is small enough to load it all initially and then just filter it client-side.
        // This means less network traffic as the data source is only hit once.
        // Note there are a couple of reports that don't use the standard DataSet and these are loaded separately.
        private static DataView dv;
        private static DataView dvNonStandard;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                // If it is a Postback then the user will already have seen any validation error message
                lblMessage.Text = "";
            }
            else
            {
                // Form initialising for first time...
                // Input validation logic
                txtKeyCode.MaxLength = 10;
                // ToolTips
                txtKeyCode.ToolTip = "Enter the Silca key code ie 'YA91'";
                lstSearchType.ToolTip =
                    "Select 'Normal' to search on the exact Key Code. 'Begins With' and 'Wildcard' will return multiple matches.";
                // Implement a responsive design.
                if (Request.Browser.IsMobileDevice || blnForceMobile)
                {
                    cssStyle.Href = "Mobile.css";
                }
                else
                {
                    cssStyle.Href = "Desktop.css";
                }
                lblMessage.Text = "";
                lblResults.Text = "";
                PopulateReportsDropDown();
                PopulateSearchTypeDropDown();
                try
                {
                    LoadDataView();
                }
                catch (Exception ex)
                {
                    lblMessage.Text = "Unable to Load Data:" + "<br />" + ex.Message;
                }
                ResetOutputPanel();
                // For Debugging:
                DisplayDataSource();
            }
        }

        private void PopulateReportsDropDown()
        {
            lstReports.Items.Add(new ListItem("All Keys Alphabetical"));
            lstReports.Items.Add(new ListItem("All Keys By Position"));
            lstReports.Items.Add(new ListItem("Board 1"));
            lstReports.Items.Add(new ListItem("Board 2"));
            lstReports.Items.Add(new ListItem("Board 3"));
            // These two reports are not part of the standard DataView
            // therefore won't be able to run them against embedded XML.
            if (!blnUseEmbeddedXMLAsDatabase)
            {
                // In the case of these reports we are mapping the drop down text to a stored proc name.
                lstReports.Items.Add(new ListItem("Shared Hooks"));
                lstReports.Items.Add(new ListItem("Multiple Hooks"));
            }
            lstReports.Items.Add(new ListItem("Non Security Keys"));
            lstReports.Items.Add(new ListItem("Security Keys"));
            // For debugging.
            // Corresponding entry in 'ReportStandard' will need un-commenting to test NoData.
            //lstReports.Items.Add(new ListItem("No Data", "NoData"));
            //lstReports.Items.Add(new ListItem("Missing Report", "MissingReport"));
        }

        private void PopulateSearchTypeDropDown()
        {
            lstSearchType.Items.Add(new ListItem("Normal"));
            lstSearchType.Items.Add(new ListItem("Begins With"));
            lstSearchType.Items.Add(new ListItem("Wildcard"));
        }

        private void LoadDataView()
        {
            KeysDAL keysDAL = new KeysDAL();

            if (blnUseEmbeddedXMLAsDatabase)
            {
                dv = new DataView(keysDAL.GetAllKeysXML());
            }
            else
            {
                keysDAL.OpenConnection(connStr);
                // Pre-filtered data not necessary
                // DataTable is small enough that we can load the whole thing and filter client side
                //dt = keysDAL.GetFilteredKeysDb(parsedKeyCode);
                dv = new DataView(keysDAL.GetAllKeysDb());
                keysDAL.CloseConnection();
            }
        }

        private void LoadDataView(string storedProcName)
        {
            KeysDAL keysDAL = new KeysDAL();
            keysDAL.OpenConnection(connStr);
            //This is a different DataSet so load it into a different object to avoid confusion.
            dvNonStandard = new DataView(keysDAL.GetReport(storedProcName));
            keysDAL.CloseConnection();
        }

        // For debugging
        private void DisplayDataSource()
        {
            string strDataSource = "";
            if (blnUseEmbeddedXMLAsDatabase)
                strDataSource = "[ EMBEDDED XML ]";
            else
                strDataSource = databaseEnvironment;
            lblDataSource.Text = strDataSource;
        }

        private void ResetOutputPanel()
        {
            // Don't want to reset this here as it may contain an error to be displayed to the user.
            // Reset it on Postback instead...
            //lblMessage.Text = "";
            lblResults.Text = "";
            pnlOutput.Visible = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ResetOutputPanel();
            if (isValidKeyCode())
            {
                string matchText;
                string filterStr = "KeyCode like ";
                parsedKeyCode = txtKeyCode.Text;

                // Filter data
                switch (lstSearchType.SelectedItem.Value)
                {
                    case "Wildcard":
                        filterStr += $"'%{parsedKeyCode}%'";
                        break;
                    case "Begins With":
                        filterStr += $"'{parsedKeyCode}%'";
                        break;
                    default:
                        filterStr += $"'{parsedKeyCode}'";
                        break;
                }

                // Need to ensure that DataView is populated before continuing
                // (ie if the form has been sitting idle for an extended period of time).
                if (dv == null)
                    try
                    {
                        LoadDataView();
                    }
                    catch (Exception ex)
                    {
                        lblMessage.Text = "Unable to Load Data:" + "<br />" + ex.Message;
                    }
                // Running a report may have reset the order of columns in the DataView so re-specify them:
                dv.RowFilter = filterStr;
                dv.Table.Columns["KeyCode"].SetOrdinal(0);
                dv.Table.Columns["Board"].SetOrdinal(1);
                dv.Table.Columns["HookNo"].SetOrdinal(2);
                dv.Table.Columns["SecurityKey"].SetOrdinal(3);
                dv.Sort = "KeyCode ASC";

                // Semantics for message string depending on if one or more than one keys found
                if (dv.Count == 1)
                    matchText = "match";
                else
                    matchText = "matches";

                // Need to check whether Data View contains records after filtering
                if (dv.Count == 0)
                {
                    switch (lstSearchType.SelectedItem.Value)
                    {
                        case "Wildcard":
                            lblMessage.Text = $"Wildcard search for '{parsedKeyCode}' returned no matches";
                            break;
                        case "Begins With":
                            lblMessage.Text = $"'Begins With' search for '{parsedKeyCode}' returned no matches";
                            break;
                        default:
                            lblMessage.Text = $"The Key Code '{parsedKeyCode}' was not found";
                            break;
                    }
                    grdKeys.DataSource = null;
                    ResetOutputPanel();
                }
                else
                {
                    switch (lstSearchType.SelectedItem.Value)
                    {
                        case "Wildcard":
                            lblResults.Text = "Wildcard search ";
                            break;
                        case "Begins With":
                            lblResults.Text = "'Begins With' search ";
                            break;
                        default:
                            lblResults.Text = "Search ";
                            break;
                    }
                    lblResults.Text += $"for '{parsedKeyCode}' returned {dv.Count} {matchText}:";

                    //Dump the view into the grid
                    grdKeys.DataSource = dv;
                    pnlOutput.Visible = true;
                }
                grdKeys.DataBind();
            }
            else
            {
                //Leave the Key Code so user can see what they entered incorrectly...
                //txtKeyCode.Text = "";
                ResetOutputPanel();
            }
        }

        private bool isValidKeyCode()
        {
            if (txtKeyCode.Text == "")
            {
                lblMessage.Text = "Please specify a Key Code.";
                return false;
            }

            // Not a Wildcard search so apply KeyCode validation criteria
            if (lstSearchType.SelectedItem.Value != "Wildcard")
            {
                if (!char.IsLetter(txtKeyCode.Text[0]))
                {
                    lblMessage.Text = "First character of the Key Code must be a letter.";
                    return false;
                }
                // Check there is a 2nd character
                else if ((txtKeyCode.Text.Length > 1))
                {
                    // Check 2nd character is not numeric
                    if (char.IsLetter(txtKeyCode.Text[1]))
                    {
                        // 2nd character is text
                        return true;
                    }
                    else
                    {
                        lblMessage.Text = "The first two characters of the Key Code must be letters.";
                        return false;
                    }
                }
                else
                    return true;
            }
            return true;
        }

        protected void btnReport_click(object sender, EventArgs e)
        {
            bool blnReportNotFound = false;
            switch (lstReports.SelectedItem.Text)
            {
                case "Shared Hooks":
                case "Multiple Hooks":
                    blnReportUsesOwnDataSet = true;
                    try
                    {
                        // The text that appears in the Dropdown is the name of the stored procedure
                        // with some spaces inserted to make it readable.
                        // Therefore just remove the spaces and you can call the stored proc.
                        LoadDataView(lstReports.SelectedItem.Text.Replace(" ",String.Empty));
                    }
                    catch (Exception ex)
                    {
                        lblMessage.Text = "Unable to Load Data:" + "<br />" + ex.Message;
                    }
                    break;
                default:
                    blnReportUsesOwnDataSet = false;
                    blnReportNotFound = ReportStandard();
                    break;
            }
            ReportOutput(blnReportNotFound);
        }

        private void ReportOutput(bool blnReportNotFound)
        {
            // There are 3 scenarios:
            // 1. The report is listed in the dropdown but no corresponding view filter or stored procedure is defined
            //    (unlikely but possible).
            // 2. The stored proc returned no data or the view filter resulted in no records in the view
            // 3. The view has data in which case display it with a message indicating how many records were returned.
            int intDataSetCount =0;

            if (blnReportUsesOwnDataSet)
                intDataSetCount = dvNonStandard.Count;
            else
                intDataSetCount = dv.Count;

            if (intDataSetCount == 0 || blnReportNotFound)
            {
                if (blnReportNotFound)
                    lblMessage.Text = "Report not found";
                else
                    lblMessage.Text = "Report returned no data";
                grdKeys.DataSource = null;
                ResetOutputPanel();
            }
            else
            {
                lblResults.Text = $"The report '{lstReports.SelectedItem.Text}' lists {intDataSetCount} Key Codes:";
                //Dump the view into the grid
                if (blnReportUsesOwnDataSet)
                    grdKeys.DataSource = dvNonStandard;
                else
                    grdKeys.DataSource = dv;
                pnlOutput.Visible = true;
            }
            grdKeys.DataBind();
        }

        private bool ReportStandard()
        {
            string filterStr = "";
            bool blnReportNotFound = false;

            // Need to ensure that DataView is populated before continuing
            // (ie if the form has been sitting idle for an extended period of time).
            if (dv==null)
                try
                {
                    LoadDataView();
                }
                catch (Exception ex)
                {
                    lblMessage.Text = "Unable to Load Data:" + "<br />" + ex.Message;
                }

            // Define the two main report formats in terms of column and row ordering:
            switch (lstReports.SelectedItem.Text)
            {
                case "All Keys By Position":
                case "Board 1":
                case "Board 2":
                case "Board 3":
                    dv.Table.Columns["Board"].SetOrdinal(0);
                    dv.Table.Columns["HookNo"].SetOrdinal(1);
                    dv.Table.Columns["KeyCode"].SetOrdinal(2);
                    dv.Table.Columns["SecurityKey"].SetOrdinal(3);
                    dv.Sort = "Board ASC, HookNo ASC";
                    break;
                default:
                    dv.Table.Columns["KeyCode"].SetOrdinal(0);
                    dv.Table.Columns["Board"].SetOrdinal(1);
                    dv.Table.Columns["HookNo"].SetOrdinal(2);
                    dv.Table.Columns["SecurityKey"].SetOrdinal(3);
                    dv.Sort = "KeyCode ASC";
                    break;
            }

            // Filter strings
            switch (lstReports.SelectedItem.Text)
            {
                case "All Keys Alphabetical":
                case "All Keys By Position":
                    filterStr = "";
                    break;
                case "Board 1":
                    filterStr = "Board = 1";
                    break;
                case "Board 2":
                    filterStr = "Board = 2";
                    break;
                case "Board 3":
                    filterStr = "Board = 3";
                    break;
                case "Non Security Keys":
                    filterStr = "SecurityKey = 0";
                    break;
                case "Security Keys":
                    filterStr = "SecurityKey = 1";
                    break;
                // Dummy entry for testing...
                //case "NoData":
                //    filterStr = "securitykey = 9";
                //    break;
                default:
                    blnReportNotFound = true;
                    break;
            }
            dv.RowFilter = filterStr;
            return blnReportNotFound;
        }

        protected void lstSearchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstSearchType.SelectedItem.Value == "Normal")
            {
                lblKeyCode.Text = "Key Code:";
                txtKeyCode.ToolTip = "Enter the Silca Key Code ie 'YA91'";
            }
            else
            {
                lblKeyCode.Text = "Search String:";
                txtKeyCode.ToolTip = "Enter letters or numbers to search on ie 'YA' or '7'.";
            }
        }
    }
}
