/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Express Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [GOLDINGS_TEST]
GO

/****** Object:  View [dbo].[VwKeyCodeComponents]    Script Date: 06/06/2018 17:17:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER view [dbo].[VwKeyCodeComponents]
as
select KeyCode,
dbo.FnGetKeyCodeNumerics(KeyCode) as KeyCodeNumerics,
dbo.FnGetKeyFamily(KeyCode) as KeyFamily
from Keys
-- from Keys_20180606
GO


