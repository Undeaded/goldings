﻿--USE [GOLDINGS_TEST]
--GO
/****** Object:  UserDefinedFunction [dbo].[FnGetKeyCodeNumerics]    Script Date: 29/08/2018 15:23:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FnGetKeyCodeNumerics] (@Temp VARCHAR(30))
-- This function is necessary so that KeyCode appears in the expected order
-- NB need to treat the numeric part of the keycode as an integer otherwise
-- field will be ordered as a varchar (ie AB12 will appear before AB6!)
RETURNS smallint AS BEGIN

	-- https://stackoverflow.com/questions/11333078/sql-take-just-the-numeric-values-from-a-varchar
    DECLARE @KeepValues AS VARCHAR(30)
    SET @KeepValues = '%[^0-9]%'
    WHILE PATINDEX(@KeepValues, @Temp) > 0
        SET @Temp = STUFF(@Temp, PATINDEX(@KeepValues, @Temp), 1, '')

    RETURN CAST(@Temp As SmallInt)
END
GO
/****** Object:  UserDefinedFunction [dbo].[FnGetKeyFamily]    Script Date: 29/08/2018 15:23:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FnGetKeyFamily] (@Temp VARCHAR(30))
RETURNS VARCHAR (30) AS BEGIN

	-- https://stackoverflow.com/questions/11333078/sql-take-just-the-numeric-values-from-a-varchar
    DECLARE @SearchValues AS VARCHAR(30),
	@KeyFamily as VarChar(30),
	@PositionOfFirstNumeric as tinyint,
	-- Default to 1 in case there isn't a space within the key code
	@PositionOfFirstSpace as tinyint = 1 
	
	-- Note percentage signs required around search string %
	-- Also circumflex ^ denotes NOT!
    SET @SearchValues = '%[0-9]%'
	SET @PositionOfFirstNumeric = PATINDEX(@SearchValues, @Temp)
	IF @PositionOfFirstNumeric > 0
		SET @KeyFamily = SUBSTRING(@Temp, 1, @PositionOfFirstNumeric-1)
	ELSE
		-- No numerics in KeyCode so take the KeyFamily as all characters up to the first space
		BEGIN 
			SET @PositionOfFirstSpace = PATINDEX('% %',@Temp)
			-- If no spaces then take the KeyFamily as the entire contents of KeyCode
			IF @PositionOfFirstSpace=0
				SET @PositionOfFirstSpace=LEN(@Temp)
			SET @KeyFamily = SUBSTRING(@Temp, 1, @PositionOfFirstSpace-1)
		END
    RETURN @KeyFamily
END

--SELECT KeyCode, PATINDEX('% %',KeyCode)
--FROM KEYS
GO
/****** Object:  Table [dbo].[Keys]    Script Date: 29/08/2018 15:23:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Keys](
	[KeyCode] [varchar](30) NOT NULL,
	[SecurityKey] [bit] NOT NULL,
	[Notes] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[KeyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VwKeyCodeComponents]    Script Date: 29/08/2018 15:23:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[VwKeyCodeComponents]
as
select KeyCode,
dbo.FnGetKeyCodeNumerics(KeyCode) as KeyCodeNumerics,
dbo.FnGetKeyFamily(KeyCode) as KeyFamily
from Keys
-- from Keys_20180606
GO
/****** Object:  Table [dbo].[Hooks]    Script Date: 29/08/2018 15:23:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hooks](
	[HookNo] [tinyint] NOT NULL,
	[Board] [tinyint] NOT NULL,
	[KeyCode] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[HookNo] ASC,
	[Board] ASC,
	[KeyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (1, 1, N'OM1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (1, 2, N'AB6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (1, 3, N'GG10')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (2, 1, N'HD2R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (2, 2, N'ADT1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (2, 3, N'GT6R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (2, 3, N'ZN1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (3, 1, N'FAT4R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (3, 2, N'ART1 HD')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (3, 3, N'YAMA 3D')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (4, 1, N'BA1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (4, 2, N'ASS2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (4, 3, N'TO30X')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (5, 1, N'CAX1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (5, 2, N'ASS6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (5, 3, N'SI1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (6, 1, N'CAX2R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (6, 2, N'ASS9')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (6, 3, N'SI4R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (7, 1, N'CIS4')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (7, 2, N'ASS31')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (7, 3, N'YAMA 5D')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (8, 1, N'CS6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (8, 2, N'ASS44')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (8, 3, N'LIC4S')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (8, 3, N'SD2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (9, 1, N'CS6R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (9, 2, N'ASS63')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (9, 3, N'GEN3')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (10, 1, N'CS7')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (10, 2, N'ASS155R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (10, 3, N'KNA1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (11, 1, N'CS32R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (11, 2, N'ASS155')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (11, 3, N'UNI19')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (11, 3, N'WST3')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (12, 1, N'CS33')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (12, 2, N'CHU6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (12, 3, N'NS10')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (13, 1, N'CS33R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (13, 2, N'COR17')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (13, 3, N'OB6R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (13, 3, N'VAC59')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (14, 1, N'BAI1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (14, 2, N'CS17')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (14, 3, N'YA8E')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (15, 1, N'ER1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (15, 2, N'CS17R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (15, 3, N'YA4R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (16, 1, N'EU1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (16, 2, N'DO1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (16, 3, N'TL1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (16, 3, N'TL2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (16, 3, N'TL2R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (17, 1, N'EU5R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (17, 2, N'DO1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (17, 3, N'TL3')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (17, 3, N'TL3R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (17, 3, N'TL4')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (17, 3, N'TL4R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (18, 1, N'EU12')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (18, 2, N'DM1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (18, 3, N'TL5')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (18, 3, N'TL5R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (18, 3, N'TL7')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (19, 1, N'EMK1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (19, 2, N'DM8')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (19, 3, N'TL8')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (19, 3, N'TL9')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (20, 1, N'FF14')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (20, 2, N'DM14')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (20, 3, N'TL10R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (21, 1, N'FF15')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (21, 2, N'DM16')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (21, 3, N'VA2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (22, 1, N'HT1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (22, 2, N'DN2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (23, 2, N'EV1S')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (24, 1, N'HW4R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (24, 2, N'EV3')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (25, 1, N'HF1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (25, 2, N'EV3R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (26, 1, N'HF74')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (26, 2, N'EV4')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (27, 1, N'MS6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (27, 2, N'EV9')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (28, 1, N'KI5')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (28, 2, N'EV10')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (29, 1, N'ER2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (29, 2, N'BUR20')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (30, 1, N'HW1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (30, 2, N'BURGH WACHTER (Shoulderless)')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (31, 1, N'HW4')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (31, 2, N'BURGH WACHTER (Long)')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (32, 1, N'IE6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (32, 2, N'BURGH WACHTER (Short)')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (33, 1, N'IE9')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (33, 2, N'RS1 HD')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (34, 1, N'JC1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (34, 2, N'ING1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (35, 1, N'JC2R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (35, 2, N'ING3')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (36, 1, N'VI7R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (36, 2, N'ING4R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (37, 1, N'RO20')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (37, 2, N'ING5')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (38, 1, N'LAP1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (38, 2, N'ING6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (39, 1, N'MLM1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (39, 2, N'HT2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (40, 1, N'MS1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (40, 2, N'RU20')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (41, 1, N'MS17')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (41, 2, N'RU21')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (42, 1, N'MC2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (42, 2, N'RU22R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (43, 1, N'LS11R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (43, 2, N'UNI32R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (44, 1, N'LS11')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (44, 2, N'UNI31')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (45, 1, N'HF75R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (45, 2, N'UNI31R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (46, 1, N'VI8R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (46, 2, N'UNI32')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (47, 1, N'VI080')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (47, 2, N'YALE HS')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (48, 1, N'VI081')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (48, 2, N'EV2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (49, 1, N'VI084')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (49, 2, N'YA86')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (50, 1, N'VI086')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (50, 2, N'YA86R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (51, 1, N'VI087')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (51, 2, N'YA2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (52, 1, N'VI13')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (52, 2, N'YA2R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (53, 1, N'UNI11B')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (53, 2, N'YA84')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (54, 1, N'YA5R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (54, 2, N'YA84R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (55, 1, N'YA91')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (55, 2, N'YA87')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (56, 1, N'YA91 Patented')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (56, 2, N'YA88')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (57, 1, N'YA6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (57, 2, N'DO2R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (58, 1, N'YA30')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (58, 2, N'KWL7')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (59, 1, N'YA4')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (59, 2, N'ARL1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (60, 1, N'YA20')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (60, 2, N'IE29')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (61, 1, N'YA18')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (61, 2, N'WBH')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (62, 1, N'YA3R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (62, 2, N'BM2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (63, 1, N'YA3')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (63, 2, N'MIL1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (64, 1, N'YA15')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (64, 2, N'YAL91L')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (65, 1, N'YA19')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (65, 2, N'DX3')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (66, 1, N'YA17')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (66, 2, N'DX3R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (67, 1, N'YA89')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (67, 2, N'BES4')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (68, 1, N'YA89')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (68, 2, N'BES9')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (69, 1, N'YA89')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (69, 2, N'BES10')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (70, 1, N'YA1ER')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (70, 2, N'TG1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (71, 1, N'CEN1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (71, 2, N'CE1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (72, 1, N'CEN1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (72, 2, N'CE1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (73, 1, N'UNI18')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (73, 2, N'CE1S')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (74, 1, N'LEG1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (74, 2, N'CE3')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (75, 1, N'SQ4R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (75, 2, N'CE4')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (76, 1, N'HEN1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (76, 2, N'CE12')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (77, 1, N'SQ5')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (77, 2, N'SH3')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (78, 1, N'SQ4R (Diagonal Head)')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (78, 2, N'CE2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (79, 1, N'SQ1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (79, 2, N'CE6R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (80, 1, N'SQ2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (80, 2, N'CE6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (81, 1, N'WEI4')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (81, 2, N'UL050X')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (82, 1, N'SQ3R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (82, 2, N'E*S5')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (83, 1, N'GN1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (83, 2, N'E*S6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (84, 1, N'UNI8')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (84, 2, N'UL051')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (85, 1, N'UNI3L')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (85, 2, N'CB6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (86, 1, N'MXU1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (86, 2, N'ERA (Brass)')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (87, 1, N'UNI1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (87, 2, N'CS204')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (88, 1, N'UNI1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (88, 2, N'TRM3')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (89, 1, N'UNI17')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (89, 2, N'CS206')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (90, 1, N'UNI27')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (90, 2, N'CS207')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (91, 1, N'UNI4')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (91, 2, N'UAP1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (92, 1, N'UNI14')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (92, 2, N'UAP2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (93, 1, N'UNI6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (93, 2, N'UL064L')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (94, 1, N'UNI28')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (94, 2, N'UL050')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (95, 1, N'UNI16')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (96, 1, N'UNI10')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (96, 2, N'UL052')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (97, 1, N'UNI7')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (97, 2, N'UL054')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (98, 1, N'UNI2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (98, 2, N'UL055')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (99, 1, N'LF2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (99, 2, N'AVC1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (100, 1, N'LF4')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (100, 2, N'NE26')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (101, 1, N'LF5')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (101, 2, N'FOT6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (102, 1, N'LF6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (102, 2, N'KS1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (103, 1, N'LF11')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (103, 2, N'UL054XL')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (104, 1, N'LF11R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (104, 2, N'TES1S')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (105, 1, N'LF13')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (105, 2, N'CLF1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (106, 1, N'LF16')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (106, 2, N'GE1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (107, 1, N'LF16R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (107, 2, N'GE106')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (108, 1, N'LF17')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (108, 2, N'AW7')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (109, 1, N'LF19')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (109, 2, N'LC2R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (110, 1, N'LF19R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (110, 2, N'TE2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (111, 1, N'LF20')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (111, 2, N'TE1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (112, 1, N'LF20R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (112, 2, N'TE1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (113, 1, N'LS14')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (113, 2, N'AB13 HD')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (114, 1, N'LF23R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (114, 2, N'AB46')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (115, 1, N'LF25')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (115, 2, N'ABUS SMALL')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (116, 1, N'LF26')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (116, 2, N'AB1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (117, 1, N'LF30R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (117, 2, N'AB10')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (118, 1, N'LF32')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (118, 2, N'AB12S')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (119, 1, N'LF32R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (119, 2, N'AB13')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (120, 1, N'LF33')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (120, 2, N'AB14')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (121, 1, N'LF45R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (121, 2, N'AB52')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (122, 1, N'LF46R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (122, 2, N'AB54')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (123, 1, N'LF53')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (123, 2, N'AB7')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (124, 1, N'LF54')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (124, 2, N'AB13R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (125, 1, N'LF7')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (125, 2, N'TE3')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (126, 1, N'YA32')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (126, 2, N'TE6')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (127, 1, N'LF27')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (127, 2, N'GHE2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (128, 1, N'LF27R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (128, 2, N'ERA2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (129, 1, N'PJ2P')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (129, 1, N'PJ3P')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (129, 2, N'CB6 HD')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (130, 1, N'STERLING (Plastic Head)')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (130, 2, N'YI13')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (131, 1, N'TL8R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (131, 2, N'HPP1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (132, 1, N'STERLING (Small Plastic Head)')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (132, 2, N'JU11R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (133, 1, N'LF6R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (133, 2, N'VI11')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (134, 1, N'CASHBOX')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (134, 2, N'JU13R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (135, 1, N'RO21')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (135, 2, N'JU12')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (136, 1, N'STERLING (Brass)')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (136, 2, N'AE1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (137, 1, N'PZ1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (137, 2, N'STA4R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (138, 1, N'CASHBOX (DS)')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (138, 2, N'TL1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (139, 1, N'LS13')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (139, 2, N'TE2R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (140, 1, N'TL7R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (141, 1, N'SZ11R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (141, 2, N'SQUIRE')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (142, 1, N'STERLING (DS)')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (142, 2, N'YI13S')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (143, 1, N'LF12')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (143, 2, N'YI14')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (143, 2, N'YI14S')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (144, 1, N'TL9R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (144, 2, N'VAC42')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (145, 1, N'TL10')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (145, 2, N'STR3')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (146, 1, N'STR2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (146, 2, N'STR4')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (147, 1, N'LF42R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (147, 2, N'STR4R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (148, 1, N'ASEC1L')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (148, 2, N'REN1')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (149, 1, N'ASEC2L')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (149, 2, N'MF1R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (150, 1, N'ASEC 20MM')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (150, 2, N'BUR2')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (151, 1, N'ASEC 25MM')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (151, 2, N'PF092S')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (152, 1, N'LS14R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (152, 2, N'LC2R')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (153, 1, N'ASEC 40/45MM')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (153, 2, N'RO8')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (154, 1, N'ASEC 40/50MM')
GO
INSERT [dbo].[Hooks] ([HookNo], [Board], [KeyCode]) VALUES (154, 2, N'IE27R')
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AB1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AB10', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AB12S', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AB13', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AB13 HD', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AB13R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AB14', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AB46', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AB52', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AB54', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AB6', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AB7', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ABUS SMALL', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ADT1', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AE1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ARL1', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ART1 HD', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASEC 20MM', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASEC 25MM', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASEC 40/45MM', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASEC 40/50MM', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASEC1L', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASEC2L', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASS155', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASS155R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASS2', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASS31', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASS44', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASS6', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASS63', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ASS9', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AVC1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'AW7', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'BA1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'BAI1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'BES10', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'BES4', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'BES9', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'BM2', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'BUR2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'BUR20', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'BURGH WACHTER (Long)', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'BURGH WACHTER (Short)', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'BURGH WACHTER (Shoulderless)', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CASHBOX', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CASHBOX (DS)', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CAX1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CAX2R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CB6', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CB6 HD', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CE1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CE12', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CE1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CE1S', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CE2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CE3', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CE4', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CE6', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CE6R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CEN1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CEN1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CHU6', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CIS4', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CLF1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'COR17', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CS17', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CS17R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CS204', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CS206', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CS207', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CS32R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CS33', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CS33R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CS6', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CS6R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'CS7', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'DM14', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'DM16', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'DM1R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'DM8', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'DN2', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'DO1', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'DO1R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'DO2R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'DX3', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'DX3R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'E*S5', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'E*S6', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'EMK1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ER1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ER2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ERA (Brass)', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ERA2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'EU12', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'EU1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'EU5R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'EV10', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'EV1S', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'EV2', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'EV3', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'EV3R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'EV4', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'EV9', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'FAT4R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'FF14', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'FF15', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'FOT6', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'GE1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'GE106', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'GEN3', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'GG10', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'GHE2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'GN1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'GT6R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'HD2R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'HEN1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'HF1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'HF74', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'HF75R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'HPP1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'HT1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'HT2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'HW1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'HW4', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'HW4R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'IE27R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'IE29', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'IE6', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'IE9', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ING1', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ING3', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ING4R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ING5', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ING6', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'JC1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'JC2R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'JU11R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'JU12', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'JU13R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'KI5', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'KNA1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'KS1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'KWL7', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LAP1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LC2R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LEG1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF11', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF11R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF12', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF13', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF16', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF16R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF17', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF19', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF19R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF20', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF20R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF23R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF25', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF26', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF27', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF27R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF30R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF32', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF32R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF33', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF4', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF42R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF45R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF46R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF5', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF53', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF54', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF6', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF6R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LF7', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LIC4S', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LS11', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LS11R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LS13', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LS14', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'LS14R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'MC2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'MF1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'MIL1', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'MLM1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'MS1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'MS17', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'MS6', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'MXU1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'NE26', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'NS10', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'OB6R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'OM1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'PF092S', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'PJ2P', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'PJ3P', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'PZ1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'REN1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'RO20', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'RO21', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'RO8', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'RS1 HD', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'RU20', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'RU21', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'RU22R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'SD2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'SH3', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'SI1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'SI4R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'SQ1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'SQ2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'SQ3R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'SQ4R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'SQ4R (Diagonal Head)', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'SQ5', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'SQUIRE', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'STA4R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'STERLING (Brass)', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'STERLING (DS)', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'STERLING (Plastic Head)', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'STERLING (Small Plastic Head)', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'STR2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'STR3', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'STR4', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'STR4R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'SZ11R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TE1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TE1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TE2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TE2R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TE3', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TE6', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TES1S', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TG1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL10', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL10R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL2R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL3', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL3R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL4', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL4R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL5', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL5R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL7', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL7R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL8', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL8R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL9', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TL9R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TO30X', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'TRM3', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UAP1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UAP2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UL050', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UL050X', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UL051', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UL052', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UL054', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UL054XL', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UL055', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UL064L', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI1', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI10', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI11B', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI14', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI16', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI17', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI18', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI19', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI1R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI27', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI28', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI31', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI31R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI32', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI32R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI3L', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI4', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI6', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI7', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'UNI8', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'VA2', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'VAC42', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'VAC59', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'VI080', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'VI081', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'VI084', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'VI086', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'VI087', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'VI11', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'VI13', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'VI7R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'VI8R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'WBH', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'WEI4', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'WST3', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA15', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA17', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA18', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA19', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA1ER', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA2', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA20', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA2R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA3', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA30', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA32', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA3R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA4', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA4R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA5R', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA6', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA84', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA84R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA86', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA86R', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA87', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA88', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA89', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA8E', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA91', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YA91 Patented', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YAL91L', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YALE HS', 1, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YAMA 3D', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YAMA 5D', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YI13', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YI13S', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YI14', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'YI14S', 0, NULL)
GO
INSERT [dbo].[Keys] ([KeyCode], [SecurityKey], [Notes]) VALUES (N'ZN1', 0, NULL)
GO
ALTER TABLE [dbo].[Hooks]  WITH CHECK ADD FOREIGN KEY([KeyCode])
REFERENCES [dbo].[Keys] ([KeyCode])
GO
/****** Object:  StoredProcedure [dbo].[AllKeysAlphabetical]    Script Date: 29/08/2018 15:23:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AllKeysAlphabetical]
AS

-- Normalise the database as HookNo and Board are not dependent on KeyCode
--select ROW_NUMBER() OVER(ORDER BY KeyCode ASC) AS Row#,
--KeyCode, SecurityKey, Board, Hook
--from Keys_20180606
--Order by Row#, KeyCode, Board, Hook

select ROW_NUMBER() OVER(ORDER BY Keys.KeyCode ASC) AS Row#,
Keys.KeyCode, SecurityKey, Board, HookNo
from Keys Inner Join Hooks on Keys.KeyCode = Hooks.KeyCode
Order by Row#, Keys.KeyCode, Board, HookNo
GO
/****** Object:  StoredProcedure [dbo].[AllKeysAlphabeticalReport]    Script Date: 29/08/2018 15:23:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AllKeysAlphabeticalReport]
AS

-- Normalise the database as HookNo and Board are not dependent on KeyCode
--select 
--IIF((SecurityKey=1), CONCAT(VwKeyCodeComponents.KeyCode, ' (S)'),VwKeyCodeComponents.KeyCode) 
--+ ' - B' + CAST(Board as varchar) 
--+ '/' + CAST(Hook as varchar) 
--As KeyCodeSec
----From VwKeyCodeComponents, Keys
--FROM VwKeyCodeComponents
----WHERE VwKeyCodeComponents.KeyCode = Keys.KeyCode
--INNER JOIN Keys_20180606 ON VwKeyCodeComponents.KeyCode = Keys_20180606.KeyCode
--Order by 
--VwKeyCodeComponents.KeyFamily, VwKeyCodeComponents.KeyCodeNumerics , Board, Hook

select IIF((SecurityKey=1), CONCAT(VwKeyCodeComponents.KeyCode, ' (S)'),VwKeyCodeComponents.KeyCode) 
+ ' - B' + CAST(Board as varchar) 
+ '/' + CAST(HookNo as varchar) 
As KeyCodeSec
--From VwKeyCodeComponents, Keys
FROM VwKeyCodeComponents
--WHERE VwKeyCodeComponents.KeyCode = Keys.KeyCode
INNER JOIN Keys ON VwKeyCodeComponents.KeyCode = Keys.KeyCode
INNER JOIN Hooks ON Keys.KeyCode = Hooks.KeyCode
Order by 
VwKeyCodeComponents.KeyFamily, VwKeyCodeComponents.KeyCodeNumerics, Board, HookNo
GO
/****** Object:  StoredProcedure [dbo].[AllKeysByPosition]    Script Date: 29/08/2018 15:23:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AllKeysByPosition]
AS

-- Normalise the database as HookNo and Board are not dependent on KeyCode
--select Board, Hook, KeyCode, SecurityKey
--from Keys_20180606
--Order by Board, Hook, KeyCode
select Board, HookNo, Keys.KeyCode, SecurityKey
from Keys Inner Join Hooks on Keys.KeyCode = Hooks.KeyCode
Order by Board, HookNo, Keys.KeyCode
GO
/****** Object:  StoredProcedure [dbo].[GetAllKeys]    Script Date: 23/10/2018 10:57:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllKeys] 
AS
-- This is effectively the KeySearch stored proc without a parameter
SELECT 
VwKeyCodeComponents.KeyCode,
Board, HookNo, SecurityKey
FROM VwKeyCodeComponents
INNER JOIN Keys ON VwKeyCodeComponents.KeyCode = Keys.KeyCode
INNER JOIN Hooks ON Keys.KeyCode = Hooks.KeyCode
--WHERE Keys.KeyCode like @KeyCode
Order by 
VwKeyCodeComponents.KeyFamily, VwKeyCodeComponents.KeyCodeNumerics, Board, HookNo
GO
GO
/****** Object:  StoredProcedure [dbo].[CreateKeysXML]    Script Date: 25/10/2018 11:55:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateKeysXML] 
AS
-- This is effectively [GetAllKeys] with a 'FOR XML' clause bolted on the end

SELECT 
VwKeyCodeComponents.KeyCode,
Board, HookNo, SecurityKey
FROM VwKeyCodeComponents
INNER JOIN Keys ON VwKeyCodeComponents.KeyCode = Keys.KeyCode
INNER JOIN Hooks ON Keys.KeyCode = Hooks.KeyCode
Order by 
VwKeyCodeComponents.KeyFamily, VwKeyCodeComponents.KeyCodeNumerics, Board, HookNo
for xml path ('KEY'), root ('KEYS')
GO

/****** Object:  StoredProcedure [dbo].[GetBoard]    Script Date: 29/08/2018 15:23:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBoard] @Board tinyint
AS
-- Normalise the database as HookNo and Board are not dependent on KeyCode
--SELECT Hook, KeyCode
--FROM Keys_20180606
--WHERE Board LIKE @Board
--ORDER BY Hook

SELECT HookNo, Hooks.KeyCode
FROM Hooks
INNER JOIN Keys ON Hooks.KeyCode = Keys.KeyCode
WHERE Board LIKE @Board
ORDER BY HookNo 
GO

/****** Object:  StoredProcedure [dbo].[GetSecurityKeys]    Script Date: 29/08/2018 15:23:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSecurityKeys]
AS
-- Normalise the database as HookNo and Board are not dependent on KeyCode
--SELECT Board, Hook, KeyCode
--FROM Keys_20180606
--WHERE SecurityKey = 1
--ORDER BY Board, KeyCode, Hook
SELECT Keys.KeyCode, Board, HookNo
FROM Keys
INNER JOIN Hooks ON Keys.KeyCode = Hooks.KeyCode
WHERE SecurityKey = 1
ORDER BY Keys.KeyCode, Board, HookNo

GO
/****** Object:  StoredProcedure [dbo].[KeySearch]    Script Date: 29/08/2018 15:23:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[KeySearch] @KeyCode varchar(30)
AS
-- Normalise the database as HookNo and Board are not dependent on KeyCode
--SELECT Keycode, Board, Hook
--FROM Keys_20180606
--WHERE KeyCode like @KeyCode

-- 20180819 Want the results to be ordered numerically within the Silca key family.
-- Need to treat the numeric part of the key code as a numeric!
--SELECT Keys.Keycode, Board, HookNo
--FROM Keys
--INNER JOIN Hooks ON Keys.KeyCode = Hooks.KeyCode
--WHERE Keys.KeyCode like @KeyCode

SELECT 
VwKeyCodeComponents.KeyCode,
Board, HookNo
FROM VwKeyCodeComponents
INNER JOIN Keys ON VwKeyCodeComponents.KeyCode = Keys.KeyCode
INNER JOIN Hooks ON Keys.KeyCode = Hooks.KeyCode
WHERE Keys.KeyCode like @KeyCode
Order by 
VwKeyCodeComponents.KeyFamily, VwKeyCodeComponents.KeyCodeNumerics, Board, HookNo
GO

/****** Object:  StoredProcedure [dbo].[MultipleHooks]    Script Date: 12/12/2018 17:50:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--ALTER PROCEDURE [dbo].[GetKeyCodeOnMoreThanOneHook]
CREATE PROCEDURE [dbo].[MultipleHooks]
AS

select K.KeyCode, Board, HookNo, SecurityKey
from Keys K
INNER JOIN Hooks H ON K.KeyCode = H.KeyCode
where K.KeyCode in 
	(select keycode
	from Hooks
	group by keycode
	having count(*)>1)
GO

/****** Object:  StoredProcedure [dbo].[SharedHooks]    Script Date: 13/12/2018 13:17:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- List hooks that hold more than one different type of key blank
--ALTER PROCEDURE [dbo].[GetHooksHoldingMultipleKeyCodes]
CREATE PROCEDURE [dbo].[SharedHooks]
AS

DROP TABLE IF EXISTS #Temp

select Board, HookNo into #Temp 
from Hooks
group by Board,HookNo
Having Count(*) > 1

select T.Board, T.HookNo, H.KeyCode, K.SecurityKey 
from #Temp T
INNER JOIN Hooks H ON T.Board = H.Board AND T.HookNo = H.HookNo
INNER JOIN Keys K ON H.KeyCode = K.KeyCode
ORDER BY Board, HookNo, KeyCode
GO