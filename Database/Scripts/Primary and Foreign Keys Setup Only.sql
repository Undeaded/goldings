
-- Primary/Foreign Key Setup
ALTER TABLE Keys ADD PRIMARY KEY (KeyCode)

ALTER TABLE Hooks ADD FOREIGN KEY (KeyCode) REFERENCES Keys(KeyCode)
ALTER TABLE Hooks ADD PRIMARY KEY (HookNo, Board, KeyCode)
