use GOLDINGS

select max(len(keycode))
from keys

select keycode, len(keycode) as longest
from keys
order by longest desc

select Board, Position, KeyCode
from keys
order by board, position

select * from keys
where keycode like 'UAP%'

select * from keys
where keycode like 'AB%'

select keycode, board, position
from keys
where keycode like 'YA%'
--order by keycode

-- Run a stored procedure
EXEC KeysGetPosition 'YA'
EXEC KeysGetPosition 'YA%'




select * from keys
where keycode like 'Patent%'

select * from keys




