USE [GOLDINGS_TEST]
GO
/****** Object:  UserDefinedFunction [dbo].[FnGetOnlyNumbers]    Script Date: 24/08/2018 11:28:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[FnGetOnlyNumbers] (@Temp VARCHAR(30))
--RETURNS VARCHAR (30) AS BEGIN
RETURNS smallint AS BEGIN

	-- https://stackoverflow.com/questions/11333078/sql-take-just-the-numeric-values-from-a-varchar
    DECLARE @KeepValues AS VARCHAR(30)
    SET @KeepValues = '%[^0-9]%'
    WHILE PATINDEX(@KeepValues, @Temp) > 0
        SET @Temp = STUFF(@Temp, PATINDEX(@KeepValues, @Temp), 1, '')

    RETURN CAST(@Temp As SmallInt)
END
