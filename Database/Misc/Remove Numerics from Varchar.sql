

CREATE FUNCTION [dbo].[GetOnlyNumbers] (@Temp VARCHAR(1000))
RETURNS VARCHAR (1000) AS BEGIN

    DECLARE @KeepValues AS VARCHAR(50)
    SET @KeepValues = '%[^0-9]%'
    WHILE PATINDEX(@KeepValues, @Temp) > 0
        SET @Temp = STUFF(@Temp, PATINDEX(@KeepValues, @Temp), 1, '')

    RETURN @Temp
END

--select KeyCode, SUBSTRING(KeyCode, PATINDEX('%[0-9]%', KeyCode), LEN(KeyCode))
--from Keys
--Order by KeyCode

--USE GOLDINGS_TEST
---- Declare a variable to return the results of the function. 
--DECLARE @ret nvarchar(15);   
---- Execute the function while passing a value to the @status parameter
--EXEC @ret = dbo.GetOnlyNumbers
--	@Temp='YA91BOLLOCKS77';
---- View the returned value.  The Execute and Select statements must be executed at the same time.  
--SELECT @ret; 


-- Call a function from within a View!vb
USE GOLDINGS_TEST
alter view VwKeyCodeNumericsOnly
as
select KeyCode,
dbo.GetOnlyNumbers(KeyCode) as KeyCodeNumerics
from Keys

--Select KeyCodeNumerics from VwKeyCodeNumericsOnly

select * from VwKeyCodeNumericsOnly
order by keyfamily, keycodenumerics


-- Need to include percentage signs around the string you are searching on! FFS!
SELECT PATINDEX('%1%','AB1')
SELECT PATINDEX('%a%', 'ddffeeaa')
SELECT PATINDEX('%ter%', 'interesting data')

select KeyCode, KeyCodeNumerics, CAST(KeyCodeNumerics as smallint) As Numeric
from VwKeyCodeNumericsOnly
order by KeyCode, Numeric 

select KeyCode,
dbo.GetOnlyNumbers(KeyCode) as KeyCodeNumerics,
dbo.GetSilcaCode(KeyCode) as SilcaCode
from Keys

select KeyCode,
dbo.GetOnlyNumbers(KeyCode) as KeyCodeNumerics
--dbo.GetSilcaCode(KeyCode) as SilcaCode
from Keys
WHERE KeyCode='AB1'



SELECT KeyCode, SUBSTRING( KeyCode ,1,PATINDEX('%[0-9]%', KeyCode )-1)
FROM Keys

SELECT KeyCode, PATINDEX('%[0-9]%', KeyCode ) as PatIndex
FROM Keys

select KeyCode, left(KeyCode, 2) from keys
order by keycode

-- Need a clause to filter out when keycode contains no numbers
select KeyCode, left(KeyCode, PATINDEX('%[0-9]%', KeyCode )-1) from keys
order by keycode





