

-- Data tidy up
-- FF5 1/21
delete from keys
where keyid = 104

--YA91 1/56
select * from keys
where keycode like 'YA91%'

delete from keys
where keyid = 332

--UL050 2/93
select * from keys
where Board = 2
and hook = 93

delete from keys
where keyid = 260

--TD30X 3/4
select * from keys
where Board = 3
and hook = 4

delete from keys
where keyid = 230

--Yale patented
select * from keys
where keycode like '%patent%'

Update Keys
set KeyCode = 'YA91 Patented'
Where KeyID = 342

update keys
set KeyCode ='UL050'
where keyid = 261 

update keys
set KeyCode ='UL050X'
where keyid = 262 
