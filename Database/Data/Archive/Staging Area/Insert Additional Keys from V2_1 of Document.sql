USE [GOLDINGS]
GO

-- 20180501
-- Insert keys added to Alphabetic key list between v2 and v2.2 of 
-- Word document

INSERT INTO [dbo].[Keys]
           ([KeyCode]
           ,[Board]
           ,[Position]
           ,[SecurityKey]
			)
     VALUES
			('Patented Yale 6-pin',1,56,0),
			('AB7',2,123,0),
			('UAP1',2,91,0);
		   
GO





