
-- Move data from Staging_Keys to Keys
-- Renamed 'BoardNumber' column to 'Board' in Keys table
Insert into Keys(KeyCode, Board, Position, SecurityKey)
Select KeyCode, BoardNumber, Position, SecurityKey
From Keys_Staging

-- KeyId smallint, not null 
--		Set up an auto_increment on this field - IDENTITY(1,1) on creation 
--		Have to do this on table creation.
-- KeyCode varchar(30), not null
-- BoardNumber tinyint, not null
-- Position tinyint, not null
-- Notes varchar(100), null