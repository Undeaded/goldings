
-- 20180502

USE GOLDINGS
BULK
INSERT Keys_Staging
-- Import file moved to OneDrive
FROM 'C:\Users\edwin\OneDrive\Goldings\Keys\CSV File for Import\Keys CSV.txt'
--FROM 'C:\Edwin\Work Stuff\Goldings\Keys\CSV File for Import\Keys CSV.txt'
WITH
(
FIRSTROW = 2,
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n'
)
GO
