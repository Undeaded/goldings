
--USE GOLDINGS_TEST

EXEC AllKeysAlphabetical
EXEC AllKeysAlphabeticalReport
EXEC AllKeysByPosition

EXEC GetBoard 1
--EXEC GetHooksHoldingMultipleKeyCodes -- Hooks with more than one Blank
EXEC SharedHooks -- Hooks with more than one Blank
--EXEC GetKeyCodeOnMoreThanOneHook -- Blanks inhabiting more than one Hook
EXEC MultipleHooks -- Blanks inhabiting more than one Hook
EXEC GetSecurityKeys
EXEC KeySearch 'CE%'
EXEC KeySearch '%CE%'
