/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [GOLDINGS_TEST]
GO
/****** Object:  StoredProcedure [dbo].[AllKeysAlphabeticalReport]    Script Date: 06/06/2018 17:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AllKeysAlphabeticalReport]
AS

-- Normalise the database as HookNo and Board are not dependent on KeyCode
--select 
--IIF((SecurityKey=1), CONCAT(VwKeyCodeComponents.KeyCode, ' (S)'),VwKeyCodeComponents.KeyCode) 
--+ ' - B' + CAST(Board as varchar) 
--+ '/' + CAST(Hook as varchar) 
--As KeyCodeSec
----From VwKeyCodeComponents, Keys
--FROM VwKeyCodeComponents
----WHERE VwKeyCodeComponents.KeyCode = Keys.KeyCode
--INNER JOIN Keys_20180606 ON VwKeyCodeComponents.KeyCode = Keys_20180606.KeyCode
--Order by 
--VwKeyCodeComponents.KeyFamily, VwKeyCodeComponents.KeyCodeNumerics , Board, Hook

select IIF((SecurityKey=1), CONCAT(VwKeyCodeComponents.KeyCode, ' (S)'),VwKeyCodeComponents.KeyCode) 
+ ' - B' + CAST(Board as varchar) 
+ '/' + CAST(HookNo as varchar) 
As KeyCodeSec
--From VwKeyCodeComponents, Keys
FROM VwKeyCodeComponents
--WHERE VwKeyCodeComponents.KeyCode = Keys.KeyCode
INNER JOIN Keys ON VwKeyCodeComponents.KeyCode = Keys.KeyCode
INNER JOIN Hooks ON Keys.KeyCode = Hooks.KeyCode
Order by 
VwKeyCodeComponents.KeyFamily, VwKeyCodeComponents.KeyCodeNumerics, Board, HookNo
