--USE [GOLDINGS_TEST]
GO
/****** Object:  StoredProcedure [dbo].[MultipleHooks]    Script Date: 12/12/2018 17:50:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--ALTER PROCEDURE [dbo].[GetKeyCodeOnMoreThanOneHook]
CREATE PROCEDURE [dbo].[MultipleHooks]
AS

select K.KeyCode, Board, HookNo, SecurityKey
from Keys K
INNER JOIN Hooks H ON K.KeyCode = H.KeyCode
where K.KeyCode in 
	(select keycode
	from Hooks
	group by keycode
	having count(*)>1)
