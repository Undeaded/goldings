﻿USE [GOLDINGS_TEST]
GO
/****** Object:  StoredProcedure [dbo].[GetAllKeys]    Script Date: 23/10/2018 10:57:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetAllKeys] 
AS
-- This is effectively the KeySearch stored proc without a parameter
SELECT 
VwKeyCodeComponents.KeyCode,
Board, HookNo, SecurityKey
FROM VwKeyCodeComponents
INNER JOIN Keys ON VwKeyCodeComponents.KeyCode = Keys.KeyCode
INNER JOIN Hooks ON Keys.KeyCode = Hooks.KeyCode
--WHERE Keys.KeyCode like @KeyCode
Order by 
VwKeyCodeComponents.KeyFamily, VwKeyCodeComponents.KeyCodeNumerics, Board, HookNo
