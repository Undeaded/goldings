/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [GOLDINGS_TEST]
GO
/****** Object:  StoredProcedure [dbo].[AllKeysAlphabetical]    Script Date: 06/06/2018 15:49:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AllKeysAlphabetical]
AS

-- Normalise the database as HookNo and Board are not dependent on KeyCode
--select ROW_NUMBER() OVER(ORDER BY KeyCode ASC) AS Row#,
--KeyCode, SecurityKey, Board, Hook
--from Keys_20180606
--Order by Row#, KeyCode, Board, Hook

select ROW_NUMBER() OVER(ORDER BY Keys.KeyCode ASC) AS Row#,
Keys.KeyCode, SecurityKey, Board, HookNo
from Keys Inner Join Hooks on Keys.KeyCode = Hooks.KeyCode
Order by Row#, Keys.KeyCode, Board, HookNo
