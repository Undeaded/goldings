﻿USE [GOLDINGS_TEST]
GO
/****** Object:  StoredProcedure [dbo].[CreateKeysXML]    Script Date: 25/10/2018 11:55:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateKeysXML] 
AS
-- This is effectively [GetAllKeys] with a 'FOR XML' clause bolted on the end

SELECT 
VwKeyCodeComponents.KeyCode,
Board, HookNo, SecurityKey
FROM VwKeyCodeComponents
INNER JOIN Keys ON VwKeyCodeComponents.KeyCode = Keys.KeyCode
INNER JOIN Hooks ON Keys.KeyCode = Hooks.KeyCode
Order by 
VwKeyCodeComponents.KeyFamily, VwKeyCodeComponents.KeyCodeNumerics, Board, HookNo
for xml path ('KEY'), root ('KEYS')
