/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [GOLDINGS_TEST]
GO
/****** Object:  StoredProcedure [dbo].[AllKeysByPosition]    Script Date: 06/06/2018 15:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AllKeysByPosition]
AS

-- Normalise the database as HookNo and Board are not dependent on KeyCode
--select Board, Hook, KeyCode, SecurityKey
--from Keys_20180606
--Order by Board, Hook, KeyCode

select Board, HookNo, Keys.KeyCode, SecurityKey
from Keys Inner Join Hooks on Keys.KeyCode = Hooks.KeyCode
Order by Board, HookNo, Keys.KeyCode
