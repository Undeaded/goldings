/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [GOLDINGS_TEST]
GO
/****** Object:  StoredProcedure [dbo].[GetBoard]    Script Date: 06/06/2018 16:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetBoard] @Board tinyint
AS
-- Normalise the database as HookNo and Board are not dependent on KeyCode
--SELECT Hook, KeyCode
--FROM Keys_20180606
--WHERE Board LIKE @Board
--ORDER BY Hook

SELECT HookNo, Hooks.KeyCode
FROM Hooks
INNER JOIN Keys ON Hooks.KeyCode = Keys.KeyCode
WHERE Board LIKE @Board
ORDER BY HookNo 
