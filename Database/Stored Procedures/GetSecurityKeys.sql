/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [GOLDINGS_TEST]
GO
/****** Object:  StoredProcedure [dbo].[GetSecurityKeys]    Script Date: 06/06/2018 16:54:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetSecurityKeys]
AS
-- Normalise the database as HookNo and Board are not dependent on KeyCode
--SELECT Board, Hook, KeyCode
--FROM Keys_20180606
--WHERE SecurityKey = 1
--ORDER BY Board, KeyCode, Hook
SELECT Keys.KeyCode, Board, HookNo
FROM Keys
INNER JOIN Hooks ON Keys.KeyCode = Hooks.KeyCode
WHERE SecurityKey = 1
ORDER BY Keys.KeyCode, Board, HookNo

