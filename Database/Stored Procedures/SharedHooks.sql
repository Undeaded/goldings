--USE [GOLDINGS_TEST]
--GO
/****** Object:  StoredProcedure [dbo].[SharedHooks]    Script Date: 13/12/2018 13:17:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- List hooks that hold more than one different type of key blank
--ALTER PROCEDURE [dbo].[GetHooksHoldingMultipleKeyCodes]
CREATE PROCEDURE [dbo].[SharedHooks]
AS

DROP TABLE IF EXISTS #Temp

select Board, HookNo into #Temp 
from Hooks
group by Board,HookNo
Having Count(*) > 1

select T.Board, T.HookNo, H.KeyCode, K.SecurityKey 
from #Temp T
INNER JOIN Hooks H ON T.Board = H.Board AND T.HookNo = H.HookNo
INNER JOIN Keys K ON H.KeyCode = K.KeyCode
ORDER BY Board, HookNo, KeyCode

