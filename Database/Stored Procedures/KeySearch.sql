USE [GOLDINGS_TEST]
GO
/****** Object:  StoredProcedure [dbo].[KeySearch]    Script Date: 19/08/2018 20:01:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[KeySearch] @KeyCode varchar(30)
AS
-- Normalise the database as HookNo and Board are not dependent on KeyCode
--SELECT Keycode, Board, Hook
--FROM Keys_20180606
--WHERE KeyCode like @KeyCode

-- 20180819 Want the results to be ordered numerically within the Silca key family.
-- Need to treat the numeric part of the key code as a numeric!
--SELECT Keys.Keycode, Board, HookNo
--FROM Keys
--INNER JOIN Hooks ON Keys.KeyCode = Hooks.KeyCode
--WHERE Keys.KeyCode like @KeyCode

SELECT 
VwKeyCodeComponents.KeyCode,
Board, HookNo
FROM VwKeyCodeComponents
INNER JOIN Keys ON VwKeyCodeComponents.KeyCode = Keys.KeyCode
INNER JOIN Hooks ON Keys.KeyCode = Hooks.KeyCode
WHERE Keys.KeyCode like @KeyCode
Order by 
VwKeyCodeComponents.KeyFamily, VwKeyCodeComponents.KeyCodeNumerics, Board, HookNo
