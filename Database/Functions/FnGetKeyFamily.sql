USE [GOLDINGS_TEST]
GO
/****** Object:  UserDefinedFunction [dbo].[GetOnlyNumbers]    Script Date: 30/05/2018 18:17:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[FnGetKeyFamily] (@Temp VARCHAR(30))
RETURNS VARCHAR (30) AS BEGIN

	-- https://stackoverflow.com/questions/11333078/sql-take-just-the-numeric-values-from-a-varchar
    DECLARE @SearchValues AS VARCHAR(30),
	@KeyFamily as VarChar(30),
	@PositionOfFirstNumeric as tinyint,
	-- Default to 1 in case there isn't a space within the key code
	@PositionOfFirstSpace as tinyint = 1 
	
	-- Note percentage signs required around search string %
	-- Also circumflex ^ denotes NOT!
    SET @SearchValues = '%[0-9]%'
	SET @PositionOfFirstNumeric = PATINDEX(@SearchValues, @Temp)
	IF @PositionOfFirstNumeric > 0
		SET @KeyFamily = SUBSTRING(@Temp, 1, @PositionOfFirstNumeric-1)
	ELSE
		-- No numerics in KeyCode so take the KeyFamily as all characters up to the first space
		BEGIN 
			SET @PositionOfFirstSpace = PATINDEX('% %',@Temp)
			-- If no spaces then take the KeyFamily as the entire contents of KeyCode
			IF @PositionOfFirstSpace=0
				SET @PositionOfFirstSpace=LEN(@Temp)
			SET @KeyFamily = SUBSTRING(@Temp, 1, @PositionOfFirstSpace-1)
		END
    RETURN @KeyFamily
END

--SELECT KeyCode, PATINDEX('% %',KeyCode)
--FROM KEYS
