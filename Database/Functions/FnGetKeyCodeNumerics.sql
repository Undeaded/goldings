/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [GOLDINGS_TEST]
GO
/****** Object:  UserDefinedFunction [dbo].[GetOnlyNumbers]    Script Date: 30/05/2018 18:17:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[FnGetKeyCodeNumerics] (@Temp VARCHAR(30))
-- This function is necessary so that KeyCode appears in the expected order
-- NB need to treat the numeric part of the keycode as an integer otherwise
-- field will be ordered as a varchar (ie AB12 will appear before AB6!)
RETURNS smallint AS BEGIN

	-- https://stackoverflow.com/questions/11333078/sql-take-just-the-numeric-values-from-a-varchar
    DECLARE @KeepValues AS VARCHAR(30)
    SET @KeepValues = '%[^0-9]%'
    WHILE PATINDEX(@KeepValues, @Temp) > 0
        SET @Temp = STUFF(@Temp, PATINDEX(@KeepValues, @Temp), 1, '')

    RETURN CAST(@Temp As SmallInt)
END
