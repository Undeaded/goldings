﻿using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace GoldingsKeysDAL
{
    public class KeysDAL
    {    
        // This member will be used by all methods
        private SqlConnection _sqlConnection;

        public void OpenConnection(string connectionString)
        {
            // Don't need a try-catch-finally as this is called by a 'using' statement 
            // which will tidy up any open connections in the event of an exception.
            _sqlConnection = new SqlConnection {ConnectionString = connectionString};
            _sqlConnection.Open();
        }

        public void CloseConnection()
        {
            _sqlConnection.Close();
        }

        public DataTable GetAllKeysXML([System.Runtime.CompilerServices.CallerFilePath]string filePath = "")
        {
            // Retrieve key data from embedded XML. All keys are returned.
            DataSet ds = new DataSet();            
            if (filePath.Contains("WindowsForms"))
            {
                // No path necessary for Windows Forms...
                ds.ReadXml("Keys.XML");
            }
            else
            {
                // For a Web app need to include the path or .NET will default to the IISExpress folder
                // and won't find the file...
                ds.ReadXml(HttpContext.Current.Server.MapPath("Keys.XML"));
            }
            return ds.Tables[0];
        }

        public DataTable GetAllKeysDb()
        {
            // Retrieve key data from the database. All keys are returned.
            DataTable dt = new DataTable();

            string storedProcName = "GetAllKeys";
            using (SqlCommand cmd = new SqlCommand(storedProcName, _sqlConnection))
            {
                //Create command object and identify it as a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    // Load the reader object into it.
                    dt.Load(dr);
                }
                dr.Close();
            }
            return dt;
        }
      
        public DataTable GetFilteredKeysDb(string parsedKeyCode)
        {
            // Retrieve key data from the database. Filtering is done in the stored proc.
            DataTable dt = new DataTable();

            string storedProcName = "KeySearch";
            using (SqlCommand cmd = new SqlCommand(storedProcName,_sqlConnection))
            {
                //Create command object and identify it as a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                //Add input parameters for the stored procedure
                cmd.Parameters.Add(new SqlParameter("@KeyCode", SqlDbType.VarChar));
                cmd.Parameters["@KeyCode"].Value = parsedKeyCode;

                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    // Load the reader object into it.
                    dt.Load(dr);
                }
                dr.Close();
            }
        return dt;
        }

        public DataTable GetReport(string storedProcName)
        {
            // Retrieve key data from the database. Filtering is done in the stored proc.
            DataTable dt = new DataTable();

            using (SqlCommand cmd = new SqlCommand(storedProcName, _sqlConnection))
            {
                //Create command object and identify it as a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    // Load the reader object into it.
                    dt.Load(dr);
                }
                dr.Close();
            }
            return dt;
        }

    }
}
